import { Component } from '@angular/core'; 
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { func } from './constants/func'; 
import { token } from './model/token';
import { cambiosBcp } from './model/cambiosBcp';
import { cambiosBcpResponse } from './model/cambiosBcpResponse';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {  

  tipoMonedas: any = ['SOL','DOL','EUR'] 

  monedaOrigen = null;
  monedaDestino = null;
  monto = null;

  tipoCambio = null;
  cambioRealizado = null;

  token : token = {} as token;
  cambiosBcp : cambiosBcp = {} as cambiosBcp;
  
  cambiosMonedaOrigen = null;
  cambiosBcpResponse : cambiosBcpResponse = {} as cambiosBcpResponse;
  cambiosRealizados : Array<cambiosBcp> = [] as Array<cambiosBcp>;


  constructor(private httpClient: HttpClient) { }




  ngOnInit() {
   
    this.generarToken();
    
  } 

  generarToken() { 
    var body = {
      "user"     : "danilo",
      "password" : "clave"
    }
 
    this.httpClient.post("http://localhost:8080/api/authenticate", 
    body,
    { headers: func.WSHeader() } )
            . subscribe(
            (response) => {  

              this.token = <token>response; 

            },
            (err: HttpErrorResponse) => {
                console.log(err.error);
                console.log(err.name);
                console.log(err.message);
                console.log(err.status);  
          },
          () => { 
            console.log("******** ocurrio un error *******");
          }
      );  
  }   
 
 

  preCalcular(){ 

    if(this.monedaOrigen!=null && this.monedaDestino!=null && this.monedaOrigen===this.monedaDestino){ 
      alert("Las monedas no pueden ser iguales. Verifique"); 
    }else{ 
      this.calcular();   
    } 
  }

  calcular(){

    var body = {
      "monto"         : this.monto,
      "monedaOrigen"  : this.monedaOrigen,
      "monedaDestino" : this.monedaDestino
    }
 
    this.httpClient.post("http://localhost:8080/v2/hacerCambioBcp", 
    body,
    { headers: func.WSHeaderToken(this.token.token) } )
            . subscribe(
            (response) => {  

              this.cambiosBcp = <cambiosBcp>response; 
              this.cambioRealizado = this.cambiosBcp.montoCambio;
              this.tipoCambio = this.cambiosBcp.tipoCambio;

              this.consultarCambiosBcp();

            },
            (err: HttpErrorResponse) => {
                console.log(err.error);
                console.log(err.name);
                console.log(err.message);
                console.log(err.status);  
          },
          () => { 
            console.log("******** ocurrio un error *******");
          }
      );   
  }
 
  consultarCambiosBcp() { 
 
    this.cambiosRealizados = [] as Array<cambiosBcp>;

    this.httpClient.get('http://localhost:8080/v2/getCambiosMonedas/'+this.cambiosMonedaOrigen,
        { headers: func.WSHeaderToken(this.token.token) })
    .subscribe(
      (data) => { 

        console.log(" response :: " + JSON.stringify(data) ); 
 
          this.cambiosBcpResponse = <cambiosBcpResponse>data;
          this.cambiosRealizados = this.cambiosBcpResponse.items;

      },
      (err: HttpErrorResponse) => {
        console.log(err.error);
        console.log(err.name);
        console.log(err.message);
        console.log(err.status); 
      } 
    );  

 
}

  limpiar(){
    this.monedaOrigen = null;
    this.monedaDestino = null;
    this.monto = null;  
    this.cambioRealizado = null;
    this.tipoCambio = null;
  }


}
