export interface cambiosBcp { 
      id            :	number,
      monto         :	number,
      monedaOrigen  :   string,
      monedaDestino :   string,
      montoCambio   :	number,
      tipoCambio    :	number
}

 